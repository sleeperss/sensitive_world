# COVID-19 : Changement de notre Organisation vers une Vie remplie d'Idées Démocratiques (let's Change our socity Organisation to a Life full of Democratic Ideas)

#### Langage Versions  
https://gitlab.com/sleeperss/sensitive_world/-/tree/readme_ **langage**   
exemple https://gitlab.com/sleeperss/sensitive_world/-/tree/readme_fr
   
#### Intro  
>We are in a war,   
A war against a common ennemy,  
Invisible,  
Which weapons are inefficient against it.  
  
>This ennemy is the madness of a whole system,  
Setted up to serve humanity,  
But in which, Humanity have been slaved by it.  
This is an ennemy that surround us,  
invisible, which affect every one of us,  
and for which talk, is the weapon that will kill it.  
<br>  
<br>  

**Let's COVID-19 to be an opportunity of changing our world.**  
<br>

For that, what's best than a decentralized Brainstorming ?  
<br>  

To make it possible, this project has two goals  
  
---> 1. **Establishing tools** to make it possible  
---> 2. In the meantime, leaving the possibility for pepole to **start sharing there own ideas**  
  
## 1. HumanStorm Tools  
   
The idea would be to take all the available tools (Blockchain, Git, etc...) to establish our idea up raiser ---> TODO :)  
  
A virus is an excellent tool of information upgrader and transmission (ADN, in case of biology), So let's take it as a starting point.  
  
The purpose is to make a tool that help us to build our ideas in the git manner (as a tree).  
And to be allowed to transmit information like a virus (based on the blockchain mechanismes).  
When a new information comes to us, we evaluate our interrest for it with a percentage, what induce two effects :  
##### - Most interesting Ideas (according to us) would be shown in forground against less interesting ones in the gui  
##### - Replication of informations would follows our interrest.
  
Like this, information that is approuved by the majority would be more shared and keept than a less collectively accepted one.  
The goal of this tool would be to **obtain in the future, one system that is mostly shared by every one and so approuved by everyone** ;)  
  
the tool project can be found here :  
https://gitlab.com/sleeperss/humanstorm  
  
   
## 2. HumanStorm Ideas  
  
Each ideas will be stored in a Markdown File, exemple : **collectivity_evaluation.md**  
exemple:  
An url to the idea will be written in the Index **README.md**  
Every branches must contains a file **fundments.md** explaining the principles of the movment.  
  
For an exemple, you can look at the idea branch :   
https://gitlab.com/sleeperss/sensitive_world/-/tree/movment-fr-openwork-decentralise  
  
To contribute, you have two possibilities :  
  
#### a. Geek choice (preffered)
  
The idea would be to build your idea as commits : one commit == une idea,  
and to edit the manifeste README.md by written the url to the idea file as explained above.  
And of course, to add the Markdown file explaining the idea.  
  
There is no master branch, every branches (préfixed by **movment-**) will be concerved as a movment.  
  
Usual contribution rules apply, merge requests of branches you want to merge on the common base node.  
  
It is, for sure, possible to cherry-pick other ideas, as well as merging two similar movments.  
  
**movment-origin** branch is the **starting point** for every other branches.  
  
thank you to prefix your branches by your langage, exemple : **movment-en**-motivation  
  
As this is a project that touch every nations, you are pleased to translate new branches into your langage (if you understand it) and to ask for merge request on the base node considered !  
As well, you are very pleased to solve raised issues by translating them into commits  
<br>

Thank you very much !   
  
#### b. The lambda humman choice

If you don't understand what I was trying to talk about 😨, to contribute, you are pleased to open an issue by explaining it and by telling to which node you want it to be attached :  
https://gitlab.com/sleeperss/sensitive_world/-/issues  
**One idea == One issue**  
  
to see **idea tree**, it's here :  
https://gitlab.com/sleeperss/sensitive_world/-/network/movment-origin  
Every points are a node of the idea tree.  
The figure shows ideas by chronological modification order (from bottom to top for most recent ones) and the links between them (branches) (red, pink and green in the image).  
  
The **root node** is **origin** :  
![alt text](graph.png "ideas tree")  
<br>
I will try to explain you how you can use it easily to see ideas. On the tree above, you have branches, that link different ideas together, if you want to see an idee, you can click the corresponding node.  
You will be redirected to the page below.  
Ok, it's a developper tool...  
If you want to see the file (without developper stuffs...), click on **View file**, as shown in the image below.  
![alt text](view_file.png "view file tuto")  
<br>

#### This README can also be corrected/translated with merge requests 😇
  
  
